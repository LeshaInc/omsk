use nalgebra::Point2;

use crate::gen::util::{BitGrid, Grid};

pub struct Params {
    pub min_road_length: f32,
}

pub struct RegionGenerator {
    generated_cells: BitGrid,
    vertices: Grid<Point2<f32>>,
    params: Params,
}

impl RegionGenerator {
    pub fn new(params: Params) -> RegionGenerator {
        RegionGenerator {
            generated_cells: Default::default(),
            vertices: Default::default(),
            params,
        }
    }
}
