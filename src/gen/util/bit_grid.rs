use fxhash::FxHashMap;
use nalgebra::Point2;

/// A 16x16 chunk of bits
#[derive(Clone, Copy, Default, Debug, Eq, PartialEq)]
pub struct BitGridChunk {
    data: [u64; 4],
}

impl BitGridChunk {
    /// Creates an empty `BitGridChunk`
    pub fn new() -> BitGridChunk {
        BitGridChunk::default()
    }

    fn index_mask(pos: Point2<u8>) -> (usize, u64) {
        debug_assert!(pos.x < 16 && pos.y < 16);
        let idx = pos.x * 16 + pos.y;
        (usize::from(idx / 64), 1 << (idx % 64))
    }

    /// Sets a bit at the specified coordinates
    pub fn set(&mut self, pos: Point2<u8>, bit: bool) {
        let (index, mask) = Self::index_mask(pos);
        if bit {
            self.data[index] |= mask;
        } else {
            self.data[index] &= !mask;
        }
    }

    /// Returns the bit at the specified coordinates
    pub fn get(&self, pos: Point2<u8>) -> bool {
        let (index, mask) = Self::index_mask(pos);
        self.data[index] & mask != 0
    }
}

/// A chunked infinite 2D bit grid (bitmap)
#[derive(Clone, Default, Debug)]
pub struct BitGrid {
    chunks: FxHashMap<Point2<i32>, BitGridChunk>,
}

impl BitGrid {
    /// Creates an empty `BitGrid`
    pub fn new() -> BitGrid {
        BitGrid::default()
    }

    /// Sets a bit at the specified coordinates
    pub fn set(&mut self, pos: Point2<i32>, bit: bool) {
        let chunk_pos = Self::cell_pos_to_chunk_pos(pos);
        let offset = Self::cell_offset_in_chunk(pos);
        let chunk = self.chunks.entry(chunk_pos).or_default();
        chunk.set(offset, bit)
    }

    /// Returns the bit at the specified coordinates
    pub fn get(&self, pos: Point2<i32>) -> bool {
        let chunk_pos = Self::cell_pos_to_chunk_pos(pos);
        let offset = Self::cell_offset_in_chunk(pos);
        self.chunks
            .get(&chunk_pos)
            .map(|chunk| chunk.get(offset))
            .unwrap_or(false)
    }

    /// Returns coordinates of a chunk, containing the specified cell
    pub fn cell_pos_to_chunk_pos(pos: Point2<i32>) -> Point2<i32> {
        pos / 16
    }

    /// Returns offset of the specified cell in its chunk
    pub fn cell_offset_in_chunk(pos: Point2<i32>) -> Point2<u8> {
        pos.map(|coord| (coord % 16) as u8)
    }

    /// Returns a shared reference to the chunk
    pub fn chunk(&self, pos: Point2<i32>) -> Option<&BitGridChunk> {
        self.chunks.get(&pos)
    }

    /// Returns a mutable reference to the chunk
    pub fn chunk_mut(&mut self, pos: Point2<i32>) -> Option<&mut BitGridChunk> {
        self.chunks.get_mut(&pos)
    }

    /// Inserts a new chunk, returning the old one
    pub fn insert_chunk(&mut self, pos: Point2<i32>, chunk: BitGridChunk) -> Option<BitGridChunk> {
        self.chunks.insert(pos, chunk)
    }

    /// Removes a chunk
    pub fn remove_chunk(&mut self, pos: Point2<i32>) -> Option<BitGridChunk> {
        self.chunks.remove(&pos)
    }

    /// Returns an iterator over all chunks
    pub fn chunks(&self) -> impl Iterator<Item = (Point2<i32>, &BitGridChunk)> + '_ {
        self.chunks.iter().map(|(k, v)| (*k, v))
    }

    /// Returns an iterator over all chunks with mutable access
    pub fn chunks_mut(&mut self) -> impl Iterator<Item = (Point2<i32>, &mut BitGridChunk)> + '_ {
        self.chunks.iter_mut().map(|(k, v)| (*k, v))
    }
}
