use fxhash::FxHashMap;
use nalgebra::Point2;

/// A 16x16 chunk of optional values
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct GridChunk<T> {
    data: [Option<T>; 256],
}

impl<T> GridChunk<T> {
    /// Creates an empty `GridChunk`
    pub fn new() -> GridChunk<T> {
        GridChunk::default()
    }

    fn index(pos: Point2<u8>) -> usize {
        debug_assert!(pos.x < 16 && pos.y < 16);
        usize::from(pos.x * 16 + pos.y)
    }

    /// Sets a value at the specified coordinates, returning the old one
    pub fn set(&mut self, pos: Point2<u8>, value: T) -> Option<T> {
        let idx = Self::index(pos);
        std::mem::replace(&mut self.data[idx], Some(value))
    }

    /// Removes the value at the specified coordinates, returning it if it was in the grid previously
    pub fn remove(&mut self, pos: Point2<u8>) -> Option<T> {
        let idx = Self::index(pos);
        std::mem::replace(&mut self.data[idx], None)
    }

    /// Returns a shared reference to the value at the specified coordinates, or None if the cell is empty
    pub fn get(&self, pos: Point2<u8>) -> Option<&T> {
        let idx = Self::index(pos);
        self.data[idx].as_ref()
    }

    /// Returns a mutable reference to the value at the specified coordinates, or None if the cell is empty
    pub fn get_mut(&mut self, pos: Point2<u8>) -> Option<&mut T> {
        let idx = Self::index(pos);
        self.data[idx].as_mut()
    }
}

impl<T> Default for GridChunk<T> {
    fn default() -> GridChunk<T> {
        GridChunk {
            data: array_init::array_init(|_| None),
        }
    }
}

/// A chunked infinite 2D grid of optional values
#[derive(Clone, Debug)]
pub struct Grid<T> {
    chunks: FxHashMap<Point2<i32>, GridChunk<T>>,
}

impl<T> Grid<T> {
    /// Creates an empty `Grid`
    pub fn new() -> Grid<T> {
        Grid::default()
    }

    /// Sets a value at the specified coordinates, returning the old one
    pub fn set(&mut self, pos: Point2<i32>, value: T) -> Option<T> {
        let chunk_pos = Self::cell_pos_to_chunk_pos(pos);
        let offset = Self::cell_offset_in_chunk(pos);
        let chunk = self.chunks.entry(chunk_pos).or_default();
        chunk.set(offset, value)
    }

    /// Removes the value at the specified coordinates, returning it if it was in the grid previously
    pub fn remove(&mut self, pos: Point2<i32>) -> Option<T> {
        let chunk_pos = Self::cell_pos_to_chunk_pos(pos);
        let offset = Self::cell_offset_in_chunk(pos);
        self.chunks
            .get_mut(&chunk_pos)
            .and_then(|chunk| chunk.remove(offset))
    }

    /// Returns a shared reference to the value at the specified coordinates, or None if the cell is empty
    pub fn get(&self, pos: Point2<i32>) -> Option<&T> {
        let chunk_pos = Self::cell_pos_to_chunk_pos(pos);
        let offset = Self::cell_offset_in_chunk(pos);
        self.chunks
            .get(&chunk_pos)
            .and_then(|chunk| chunk.get(offset))
    }

    /// Returns a mutable reference to the value at the specified coordinates, or None if the cell is empty
    pub fn get_mut(&mut self, pos: Point2<i32>) -> Option<&mut T> {
        let chunk_pos = Self::cell_pos_to_chunk_pos(pos);
        let offset = Self::cell_offset_in_chunk(pos);
        self.chunks
            .get_mut(&chunk_pos)
            .and_then(|chunk| chunk.get_mut(offset))
    }

    /// Returns coordinates of a chunk, containing the specified cell
    pub fn cell_pos_to_chunk_pos(pos: Point2<i32>) -> Point2<i32> {
        pos / 16
    }

    /// Returns offset of the specified cell in its chunk
    pub fn cell_offset_in_chunk(pos: Point2<i32>) -> Point2<u8> {
        pos.map(|coord| (coord % 16) as u8)
    }

    /// Returns a shared reference to the chunk
    pub fn chunk(&self, pos: Point2<i32>) -> Option<&GridChunk<T>> {
        self.chunks.get(&pos)
    }

    /// Returns a mutable reference to the chunk
    pub fn chunk_mut(&mut self, pos: Point2<i32>) -> Option<&mut GridChunk<T>> {
        self.chunks.get_mut(&pos)
    }

    /// Inserts a new chunk, returning the old one
    pub fn insert_chunk(&mut self, pos: Point2<i32>, chunk: GridChunk<T>) -> Option<GridChunk<T>> {
        self.chunks.insert(pos, chunk)
    }

    /// Removes a chunk
    pub fn remove_chunk(&mut self, pos: Point2<i32>) -> Option<GridChunk<T>> {
        self.chunks.remove(&pos)
    }

    /// Returns an iterator over all chunks
    pub fn chunks(&self) -> impl Iterator<Item = (Point2<i32>, &GridChunk<T>)> + '_ {
        self.chunks.iter().map(|(k, v)| (*k, v))
    }

    /// Returns an iterator over all chunks with mutable access
    pub fn chunks_mut(&mut self) -> impl Iterator<Item = (Point2<i32>, &mut GridChunk<T>)> + '_ {
        self.chunks.iter_mut().map(|(k, v)| (*k, v))
    }
}

impl<T> Default for Grid<T> {
    fn default() -> Grid<T> {
        Grid {
            chunks: FxHashMap::default(),
        }
    }
}
