use std::f32::consts::TAU;

use rand::Rng;

use nalgebra::{Point2, Vector2};

pub trait Area {
    fn contains_point(&self, point: Point2<f32>) -> bool;

    fn contains_cell(&self, cell_size: f32, cell: Point2<i32>) -> bool;

    fn rasterize<F: FnMut(Point2<i32>)>(&self, cell_size: f32, f: F);

    fn sample<R: Rng>(&self, rng: &mut R) -> Point2<f32>;
}

pub struct CircularArea {
    pub origin: Point2<f32>,
    pub radius: f32,
}

impl Area for CircularArea {
    fn contains_point(&self, point: Point2<f32>) -> bool {
        (point - self.origin).map(|v| v.powi(2)).sum() <= self.radius
    }

    fn contains_cell(&self, cell_size: f32, cell: Point2<i32>) -> bool {
        // TODO: what if the cell is bigger than the area?
        let point = cell.map(|v| v as f32 * cell_size);
        self.contains_point(point)
            || self.contains_point(point + Vector2::new(0.0, 1.0))
            || self.contains_point(point + Vector2::new(1.0, 0.0))
            || self.contains_point(point + Vector2::new(1.0, 1.0))
    }

    fn rasterize<F: FnMut(Point2<i32>)>(&self, cell_size: f32, mut f: F) {
        let xy_radius = Vector2::from_element(self.radius);
        let min = self.origin - xy_radius;
        let max = self.origin + xy_radius;

        let to_cell = |v: f32| (v / cell_size).floor() as i32;
        let min = min.map(to_cell);
        let max = max.map(to_cell);

        for cell_x in min.x..=max.x {
            for cell_y in min.y..=max.y {
                f(Point2::new(cell_x, cell_y));
            }
        }
    }

    fn sample<R: Rng>(&self, rng: &mut R) -> Point2<f32> {
        let angle = rng.gen_range(0.0, TAU);
        let offset = rng.gen::<f32>().powi(2) * self.radius;
        let (sin, cos) = angle.sin_cos();
        self.origin + Vector2::new(offset * cos, offset * sin)
    }
}
