use std::f32::consts::{FRAC_1_SQRT_2, TAU};

use nalgebra::Point2;
use rand::Rng;

use crate::gen::area::Area;
use crate::gen::util::{BitGrid, Grid};

pub fn sample_circle<R: Rng>(r: f32, rng: &mut R) -> Point2<f32> {
    let angle = rng.gen_range(0.0, TAU);
    let offset = rng.gen::<f32>().powi(2) * r;
    let (sin, cos) = angle.sin_cos();
    Point2::new(offset * cos, offset * sin)
}

pub fn sample_disk<R: Rng>(ar: f32, br: f32, rng: &mut R) -> Point2<f32> {
    let angle = rng.gen_range(0.0, TAU);
    let offset = rng.gen_range((ar / br).sqrt(), 1.0).powi(2) * br;
    let (sin, cos) = angle.sin_cos();
    Point2::new(offset * cos, offset * sin)
}

pub struct PoissonDiscSampling {
    radius: f32,
    radius2: f32,
    cell_size: f32,
    visited_cells: BitGrid,
    point_grid: Grid<Point2<f32>>,
    active_samples: Vec<Point2<f32>>,
}

impl PoissonDiscSampling {
    pub fn new(radius: f32) -> PoissonDiscSampling {
        PoissonDiscSampling {
            radius,
            radius2: radius.powi(2),
            cell_size: radius * FRAC_1_SQRT_2,
            visited_cells: BitGrid::new(),
            point_grid: Grid::new(),
            active_samples: Vec::new(),
        }
    }

    //  TODO: replace with generic area
    pub fn generate<A: Area, R: Rng>(&mut self, area: &A, rng: &mut R) {
        self.active_samples.clear();

        area.rasterize(self.cell_size, |cell| {
            if let Some(sample) = self.point_grid.get(cell) {
                self.active_samples.push(*sample);
            }
        });

        area.rasterize(self.cell_size, |cell| {
            self.visited_cells.set(cell, true);
        });

        if self.active_samples.is_empty() {
            let sample = area.sample(rng);
            self.active_samples.push(sample);
        }

        'outer: while !self.active_samples.is_empty() {
            let idx = if self.active_samples.len() == 1 {
                0
            } else {
                rng.gen_range(0, self.active_samples.len() - 1)
            };

            let sample = self.active_samples[idx];

            for _ in 0..10 {
                let new_sample = sample + sample_disk(self.radius, self.radius * 2.0, rng).coords;
                if !area.contains_point(new_sample) {
                    continue;
                }

                let to_cell = |v: f32| (v / self.cell_size).floor() as i32;
                let cx = to_cell(new_sample.x);
                let cy = to_cell(new_sample.y);

                for sx in cx - 2..=cx + 2 {
                    for sy in cy - 2..=cy + 2 {
                        if let Some(&other_cell) = self.point_grid.get([sx, sy].into()) {
                            if (other_cell - new_sample).map(|v| v.powi(2)).sum() < self.radius2 {
                                continue;
                            }
                        }
                    }
                }

                self.point_grid.set([cx, cy].into(), new_sample);
                self.active_samples.push(new_sample);
                continue 'outer;
            }

            self.active_samples.swap_remove(idx);
        }
    }
}
